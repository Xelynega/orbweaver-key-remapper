#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/input.h>
#include <linux/uinput.h>
#include <unistd.h>
#include <string.h>

#define nullptr 0
#define true 1
#define false 0

#define ORBWEAVER_PROFILE_MIN_KEYCODE    0xfff7
#define ORBWEAVER_PROFILE_MAX_KEYCODE    0xfffd
#define ORBWEAVER_CYCLE_PROFILE_KEYCODE  0xfffe

// Constants
const unsigned short event_codes[26] = {41, 2, 3, 4, 5, 15, 16, 17, 18, 19, 58, 30, 31, 32, 33, 42, 44, 45, 46, 47, 56, 103, 106, 108, 105, 57};

const struct input_event sync_event = {.type = EV_SYN, .value = 0, .code = 0};

// Globals
unsigned short*   orbweaver_maps[7] = {nullptr};
unsigned char     orbweaver_colors[7][3] = {{0}};
int               num_orbweaver_maps = 0;
int               current_orbweaver_map = 0;
char              orbweaver_sysfs[1024] = "/sys";
char*             led_1_file = nullptr;
char*             led_2_file = nullptr;
char*             led_3_file = nullptr;
char*             static_effect_file = nullptr;

int get_orbweaver_event_id(char ** sysfs_dest)
{
	FILE* devices_fp = fopen("/proc/bus/input/devices", "r");
	int event_num = -1;

	char* line_buffer = NULL;
	size_t line_size;
	ssize_t read;

	char sysfs_found = false;
	char* sysfs_temp = (char *)malloc(1024);

	while((read = getline(&line_buffer, &line_size, devices_fp)) > 0 && (event_num == -1 || sysfs_found == false))
	{
		if(strcmp(&line_buffer[12], "Vendor=1532 Product=0207 Version=0111\n") == 0)
		{
			free(line_buffer);
			line_buffer = NULL;
			while((read = getline(&line_buffer, &line_size, devices_fp)) > 0)
			{
				if(line_buffer[0] == '\n')
				{
					free(line_buffer);
					line_buffer = NULL;
					break;
				}
				if(line_buffer[0] == 'S')
				{
					strcpy(sysfs_temp, &line_buffer[9]);
				}
				if(line_buffer[0] == 'H')
				{
					if(strstr(line_buffer, "mouse") != 0)
					{
						*sysfs_dest = sysfs_temp;
						sysfs_found = true;
					}
					if(strncmp(line_buffer, "H: Handlers=sysrq kbd event", 27) == 0 && strstr(line_buffer, "leds") != 0)
					{
						char* event_token = strtok(&line_buffer[27], " ");
						event_num = atoi(event_token);
					}
					free(line_buffer);
					line_buffer = NULL;
					break;
				}
				free(line_buffer);
				line_buffer = NULL;
			}
		}
		else
		{
			free(line_buffer);
			line_buffer = NULL;
		}
	}

	fclose(devices_fp);
	return event_num;
}

int setup_uinput_device()
{
	int fd_uinput = open("/dev/uinput", O_WRONLY | O_NONBLOCK);

	struct uinput_user_dev dev_keyboard_emulator;
	memset(&dev_keyboard_emulator, 0, sizeof(struct uinput_user_dev));
	snprintf(dev_keyboard_emulator.name, UINPUT_MAX_NAME_SIZE, "orbweaver key remapper");
	dev_keyboard_emulator.id.bustype = BUS_USB;
	dev_keyboard_emulator.id.vendor = 0x1532;
	dev_keyboard_emulator.id.product = 0x0207;
	dev_keyboard_emulator.id.version = 1;

	if(ioctl(fd_uinput, UI_SET_EVBIT, EV_KEY) < 0)
	{
		printf("Failed to ioctl UI_SET_EVBIT\n");
		return -1;
	}
	
	for(int iEvent = 0; iEvent < 254; iEvent++)
	{
		if(ioctl(fd_uinput, UI_SET_KEYBIT, iEvent) < 0)
		{
			printf("Failed to set UI_SET_KEYBIT for event %d\n", iEvent);
		}
	}

	if(ioctl(fd_uinput, UI_SET_EVBIT, EV_SYN) < 0)
	{
		printf("Failed to ioctl UI_SET_EVBIT\n");
		return -1;
	}

	if(write(fd_uinput, &dev_keyboard_emulator, sizeof(struct uinput_user_dev)) < 0)
	{
		printf("Failed to write device to uinput\n");
		return -1;
	}

	if(ioctl(fd_uinput, UI_DEV_CREATE) < 0)
	{
		printf("Failed to ioctl UI_DEV_CREATE\n");
		return -1;
	}

	return fd_uinput;
}

int open_orbweaver_event_dev(int orbweaver_event_id)
{
	int fd_orbweaver;
	char event_filename[256];
	snprintf(event_filename, 256, "/dev/input/event%d", orbweaver_event_id);
	if((fd_orbweaver = open(event_filename, O_RDWR)) < 0)
	{
		printf("Failed to open orbweaver event file at %s\n", event_filename);
		return -1;
	}
	return fd_orbweaver;
}

int read_orb_file(char* filename, unsigned short* map_array, unsigned char* color_array)
{
	int fd_orb;
	if((fd_orb = open(filename, O_RDONLY)) < 0)
	{
		printf("Failed to open %s\n", filename);
		return -1;
	}
	
	unsigned char buffer[55] = {0};
	if(read(fd_orb, (char *)buffer, 55) != 55)
	{
		memset((void*)map_array, 0, 52);
		memset((void*)color_array, 0, 3);
		return -1;	
	}
	else
	{
		memcpy((void*)map_array, (void*)buffer, 52);
		memcpy((void*)color_array, (void*)&buffer[52], 3);
	}
	
	return 0;
}

unsigned int event_code_to_index(unsigned short event_code)
{
	for(unsigned int i = 0; i < 26; i++)
	{
		if(event_codes[i] == event_code)
		{
			return i;
		}
	}
	return -1;
}

// profile_led_blue -> profile_led_red -> profile_led_green
void switch_orbweaver_map(unsigned int profile)
{
	// Check if the map that is being switched to exists. If it does, set the current profile to it and set the LEDS, if it doesn't return
	if(profile >= 7)
	{
		return;
	}
	
	if(orbweaver_maps[profile] == nullptr)
	{
		return;
	}

	if(led_1_file == nullptr)
	{
		led_1_file = (char *)malloc(1024);
		led_2_file = (char *)malloc(1024);
		led_3_file = (char *)malloc(1024);
		static_effect_file = (char *)malloc(1024);

		strcpy(led_1_file, orbweaver_sysfs);
		strcpy(led_2_file, orbweaver_sysfs);
		strcpy(led_3_file, orbweaver_sysfs);
		strcpy(static_effect_file, orbweaver_sysfs);

		strcat(led_1_file, "/profile_led_blue");
		strcat(led_2_file, "/profile_led_red");
		strcat(led_3_file, "/profile_led_green");
		strcat(static_effect_file, "/matrix_effect_static");
	}

	int led_1_fd = open(led_1_file, O_WRONLY);
	int led_2_fd = open(led_2_file, O_WRONLY);
	int led_3_fd = open(led_3_file, O_WRONLY);
	int static_effect_fd = open(static_effect_file, O_WRONLY);

	char on = '1';
	char off = '0';

	write(led_1_fd, (((profile + 1) & 0b001) ? &on : &off), 1);
	write(led_2_fd, (((profile + 1) & 0b010) ? &on : &off), 1);
	write(led_3_fd, (((profile + 1) & 0b100) ? &on : &off), 1);
	write(static_effect_fd, orbweaver_colors[profile], 3);

	close(led_1_fd);
	close(led_2_fd);
	close(led_3_fd);
	close(static_effect_fd);

	current_orbweaver_map = profile;
}


void cycle_orbweaver_map()
{
	if(current_orbweaver_map + 1 == num_orbweaver_maps)
	{
		switch_orbweaver_map(0);
		return;
	}
	switch_orbweaver_map(current_orbweaver_map + 1);
}

int parse_orbweaver_event(struct input_event orbweaver_event[3], int uinput_fd)
{
	// Map even to current profile key, if not special then send event
	int key_event = 0;
	while(key_event < 3)
	{
		if(orbweaver_event[key_event].type == 1)
		{
			break;
		}
		orbweaver_event++;
	}

	struct input_event generated_key_event;
	generated_key_event.code = orbweaver_maps[current_orbweaver_map][event_code_to_index(orbweaver_event[key_event].code)];
	if(generated_key_event.code == 0)
	{
		return 0;
	}

	if(generated_key_event.code >= ORBWEAVER_PROFILE_MIN_KEYCODE && generated_key_event.code <= ORBWEAVER_PROFILE_MAX_KEYCODE && orbweaver_event[key_event].value == 0)
	{
		switch_orbweaver_map(generated_key_event.code - ORBWEAVER_PROFILE_MIN_KEYCODE);
		return 0;
	}

	if(generated_key_event.code == ORBWEAVER_CYCLE_PROFILE_KEYCODE && orbweaver_event[key_event].value == 0)
	{
		cycle_orbweaver_map();
		return 0;
	}

	generated_key_event.type = EV_KEY;
	generated_key_event.value = orbweaver_event[key_event].value;
	write(uinput_fd, &generated_key_event, sizeof(struct input_event));
	write(uinput_fd, &sync_event, sizeof(struct input_event));

	return 0;
}

int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		printf("orb file required\n");
		return 1;
	}

	while(num_orbweaver_maps < (argc - 1) && num_orbweaver_maps < 7)
	{
		orbweaver_maps[num_orbweaver_maps] = (unsigned short *)malloc(26 * sizeof(unsigned short));
		if(read_orb_file(argv[num_orbweaver_maps + 1], orbweaver_maps[num_orbweaver_maps], orbweaver_colors[num_orbweaver_maps]) < 0)
		{
			printf("Failed to read orbweaver map %d: %s\n", num_orbweaver_maps + 1, argv[num_orbweaver_maps + 1]);
			return -1;
		}

		num_orbweaver_maps++;
	}
	char* orbweaver_sysfs_temp;
	int orbweaver_event_id = get_orbweaver_event_id(&orbweaver_sysfs_temp);
	if(orbweaver_event_id < 0)
	{
		printf("Could not find any attached orbweavers\n");
		return 1;
	}

	strcpy(&orbweaver_sysfs[4], orbweaver_sysfs_temp);
	orbweaver_sysfs[strlen(orbweaver_sysfs) - 1] = '\0';
	strcat(orbweaver_sysfs, "/device");
	int fd_keyboard = setup_uinput_device();
	int fd_orbweaver = open_orbweaver_event_dev(orbweaver_event_id);
	switch_orbweaver_map(0);

	while(1)
	{
		struct input_event orbweaver_event[3];
		ssize_t size = read(fd_orbweaver, orbweaver_event, sizeof(struct input_event) * 3);
		(void)size;

		parse_orbweaver_event(orbweaver_event, fd_keyboard);

		//printf("Code[0]: %hu\n\tType[0]: %hu\nCode[1]: %hu\n\tType[1]: %hu\nCode[2]: %hu\n\n", orbweaver_event[0].code, orbweaver_event[0].type, orbweaver_event[1].code, orbweaver_event[1].type, orbweaver_event[2].code);

		//struct input_event kb_event;
		//kb_event.type = EV_KEY;
		//kb_event.value = orbweaver_event[1].value;
		//kb_event.code = orbweaver_event[1].code;
		//write(fd_keyboard, &kb_event, sizeof(struct input_event));
		//kb_event.type = EV_SYN;
		//kb_event.value = 0;
		//kb_event.code = 0;
		//write(fd_keyboard, &kb_event, sizeof(struct input_event));
	}

	return 0;
}
